#!/usr/bin/env bash

SYS_TYPE=$(uname -s)

if [[ $SYS_TYPE = "Darwin" ]]
then
  port_cmd=$(which port)
  if [[ -x "$port_cmd" ]]
  then
    sudo $port_cmd selfupdate
    sudo $port_cmd upgrade outdated
  fi
  brew_cmd=$(which brew)
  if [[ -x "$brew_cmd" ]]
  then
    $brew_cmd upgrade
  fi
  rustup_cmd=$(which rustup)
  if [[ -x "$rustup_cmd" ]]
  then
    $rustup_cmd update
  fi
fi
